
#######################################################################
# Process to build new release
# Triggered when a branch prefixed by release/ will be pushed
# Will update code and create a merge request targeting dev
#######################################################################

initialize-ssh:
  stage: pretooling
  only:
    # prefixed by release /^release/.*/
    - /^release/.*/
    - tags
  script:
      - chmod 766 ~/.ssh
      - eval `ssh-agent -s`
      - cat $SSH_PRIVATE_KEY_TOOLKIT  > ~/.ssh/id_rsa
      - chmod 600 ~/.ssh/id_rsa
      - ssh-keyscan gitlab.com  >> ~/.ssh/known_hosts
      - chmod 644 ~/.ssh/known_hosts

initialize-git:
  stage: pretooling
  dependencies:
    - initialize-ssh
  only:
    # prefixed by release /^release/.*/
    - /^release/.*/
    - tags
  script:
      - git config --global user.name 'InfraBot'
      - git config --global user.email 'infra@marigold.dev'

build_release_env_vars:
  stage: pretooling
  script:
    - RELEASE_VERSION=${CI_COMMIT_BRANCH#"release/"}
    - echo "RELEASE_VERSION=${RELEASE_VERSION}" >> releaseversion.env
  artifacts:
    reports:
      dotenv: releaseversion.env
  rules:
    - if: ($CI_COMMIT_BRANCH =~ /^release/ )
      when: always
    - if: ($CI_COMMIT_BRANCH =~ /^release/ && $DRY_RUN_RELEASE != null)
      when: always
#######################################################################
# Following script is executed when branch is create
#######################################################################
create-tag:
  stage: release    
  script:
    - git remote rm origin && git remote add origin "git@gitlab.com:ligolang/ligo.git"
    # Don't want to tag for testing
    - echo $DRY_RUN_RELEASE
    - |
      if [[ $DRY_RUN_RELEASE == "" ]]; then
        git tag -a $RELEASE_VERSION -m "Tag version $RELEASE_VERSION"
        git push origin $RELEASE_VERSION
      fi
  needs:
    - job: initialize-ssh
    - job: initialize-git
    - job: build_release_env_vars
      artifacts: true
  rules:
    - if: '$CI_COMMIT_BRANCH =~ /^release/ && $TRIGGER_DOC_UPDATE == null' 
      when: always
    - if: ($CI_COMMIT_BRANCH =~ /^release/ && $DRY_RUN_RELEASE != null)
      when: always
#######################################################################
# Following script is executed when pipeline execute the release tag process
# Related to release generation
# Related to stable version
# Related to metadata update
#######################################################################
determine_new_stable_version:
  stage: pretooling
  image: node:12.20.0-buster
  rules:
  - if: '$CI_COMMIT_TAG =~ /[0-9]+\.[0-9]+\.0/'
    when: always
  - if: ($CI_COMMIT_BRANCH =~ /^release/ && $DRY_RUN_RELEASE != null)
    when: always
  tags:
    - docker
  before_script:
    # Have to be done here because we are using an image for this job.
    - apt-get update
    - apt-get install -y --no-install-recommends jq
  artifacts:
    reports:
      dotenv: stableversion.env
  script:
  # Determine new stable version from website docs.
  # If we stop to use docusaurus we have to determine this value by another way
    - STABLE_VERSION=`jq '.[1]' gitlab-pages/website/versions.json`
    - echo "STABLE_VERSION=${STABLE_VERSION}" >> stableversion.env

release:
  stage: push
  image: registry.gitlab.com/gitlab-org/release-cli
  rules:
    - if: '$CI_COMMIT_TAG =~ /[0-9]+\.[0-9]+\.[0-9]+/'
      when: always
    - if: ($CI_COMMIT_BRANCH =~ /^release/ && $DRY_RUN_RELEASE != null)
      when: always
  dependencies:
    - docker_extract
    - changelog
    - initialize-git
  ## FIXME find a better solution to upload the binary
  artifacts:
    expire_in: 1000 yrs
    paths:
      - ligo
      - ligo.deb
    reports:
      dotenv: releaseCiJobId.env
  script:
    - |
      if [[ $DRY_RUN_RELEASE == "" ]]; then
        release-cli create --name "Release $CI_COMMIT_TAG" --description "$(cat release-notes.md)" --assets-links-name "Static Linux binary" --assets-links-url "$CI_PROJECT_URL/-/jobs/$CI_JOB_ID/artifacts/raw/ligo" --assets-links-name "deb package" --assets-links-url "$CI_PROJECT_URL/-/jobs/$CI_JOB_ID/artifacts/raw/ligo.deb"
      fi
    - echo "RELEASE_CI_JOB_ID=${CI_JOB_ID}" >> releaseCiJobId.env

create_docker_stable_version:
  stage: push
  rules:
    - if: '$CI_COMMIT_TAG =~ /[0-9]+\.[0-9]+\.0/'
      when: always
    - if: ($CI_COMMIT_BRANCH =~ /^release/ && $DRY_RUN_RELEASE != null)
      when: always
  needs:
    - job: determine_new_stable_version
      artifacts: true
  script:
    - STABLE_VERSION_WITHOUT_QUOTE=$(echo ${STABLE_VERSION} | tr -d '"')
    - docker pull ligolang/ligo:$STABLE_VERSION_WITHOUT_QUOTE
    - |
      if [[ $DRY_RUN_RELEASE == "" ]]; then
        docker tag ligolang/ligo:$STABLE_VERSION_WITHOUT_QUOTE ligolang/ligo:stable
        docker push ligolang/ligo:stable
      fi

update_release_metadata:
  stage: push
  rules:
    - if: '$CI_COMMIT_TAG =~ /[0-9]+\.[0-9]+\.0/'
      when: always
    - if: ($CI_COMMIT_BRANCH =~ /^release/ && $DRY_RUN_RELEASE != null)
      when: always
  needs:
    - job: release
    - job: determine_new_stable_version
      artifacts: true
  script:
      # git cli initialized in .ci/gitlab-ci.yml. Update release branch metadata
    - |      
      if [[ $DRY_RUN_RELEASE != "" ]]; then
        CI_COMMIT_TAG=${CI_COMMIT_BRANCH#"release/"}
      fi
    - git remote rm origin && git remote add origin "git@gitlab.com:ligolang/ligo.git" 
    - git fetch
    - git checkout release/$CI_COMMIT_TAG  
    - ./.ci/scripts/release_metadata/edit_release_metadata.sh LAST_TAG_JOB_ID $RELEASE_CI_JOB_ID 
    - ./.ci/scripts/release_metadata/edit_release_metadata.sh LAST_STABLE_VERSION $STABLE_VERSION 
    - git commit -am "[Bot] Update release metadata $CI_COMMIT_TAG"
    - git push -o ci.variable="TRIGGER_DOC_UPDATE='true'" origin HEAD:release/$CI_COMMIT_TAG

#######################################################################
# Following script is executed when pipeline has executed Tag process
# Related to documentation and web-ide
#######################################################################
update-versionned-doc:
  stage: tooling  
  image: node:16.17.0-buster
  tags:
    - docker
  before_script:
    # Have to be done here because we are using an image for this job.
    - mkdir ~/.ssh
    - chmod 766 ~/.ssh
    - eval `ssh-agent -s`
    - cat $SSH_PRIVATE_KEY_TOOLKIT  > ~/.ssh/id_rsa
    - chmod 600 ~/.ssh/id_rsa
    - ssh-keyscan gitlab.com  >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - git config --global user.name 'InfraBot'
    - git config --global user.email 'infra@marigold.dev'
    - apt-get update
    - apt-get install -y --no-install-recommends jq
    - wget https://github.com/mikefarah/yq/releases/download/v4.18.1/yq_linux_amd64 -O /usr/bin/yq && chmod +x /usr/bin/yq
  script:
    - git remote rm origin && git remote add origin "git@gitlab.com:ligolang/ligo.git"
    - node scripts/manpages/manpages.js ./ligo
    - ./.ci/scripts/versioning_website.sh $RELEASE_VERSION
    - LAST_TAG_JOB_ID=`./.ci/scripts/release_metadata/read_release_metadata.sh LAST_TAG_JOB_ID`
    - ./.ci/scripts/update_distribution_references_with_release.sh $LAST_TAG_JOB_ID $RELEASE_VERSION
    - git add nix/get_ligo.nix gitlab-pages/docs/intro/installation.md gitlab-pages/website/versioned_docs/. gitlab-pages/website/versioned_sidebars/. gitlab-pages/website/versions.json tools/webide/Dockerfile gitlab-pages/docs/manpages
    - git commit -m "[Bot] Docs $RELEASE_VERSION"
    - git push -o ci.skip origin HEAD:$CI_COMMIT_REF_NAME
  needs:
    - job: docker_extract
    - job: build_release_env_vars
      artifacts: true
  rules:
    - if: ($CI_COMMIT_BRANCH =~ /^release/ &&  $TRIGGER_DOC_UPDATE != null) 
      when: always
    - if: ($CI_COMMIT_BRANCH =~ /^release/ && $DRY_RUN_RELEASE != null)
      when: always

create-doc-update-MR:
  stage: tooling
  dependencies:
    - update-versionned-doc
  script:
    - HOST=${CI_PROJECT_URL} CI_PROJECT_ID=${CI_PROJECT_ID} CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME} PRIVATE_TOKEN=${TOKEN_API} ./.ci/scripts/auto_merge_request.sh
  rules:
    - if: ($CI_COMMIT_BRANCH =~ /^release/ &&  $TRIGGER_DOC_UPDATE != null )
      when: always
    - if: ($CI_COMMIT_BRANCH =~ /^release/ && $DRY_RUN_RELEASE != null)
      when: always
#######################################################################
# Following script is executed when pipeline has executed Tag process
# Related to the update of Gitpod
#######################################################################

update-gitpod-ligo-version:
  stage: tooling
  variables:
    RELEASE_VERSION: "${RELEASE_VERSION}"
  trigger: 
    project: ligolang/template-ligo
  needs:
    - job: build_release_env_vars
      artifacts: true
  rules:
    - if: ($CI_COMMIT_BRANCH =~ /^release/ &&  $TRIGGER_DOC_UPDATE != null )
      when: always 
