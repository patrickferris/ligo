
### SYNOPSIS
```
ligo publish
```

### DESCRIPTION
[BETA] Packs the pacakage directory contents into a tarball and uploads it to the registry server

### FLAGS
**--ligorc-path PATH**
path to gobal .ligorc file.

**--project-root PATH**
The path to root of the project.

**--registry URL**
The url to a LIGO registry.

**-help**
print this help text and exit (alias: -?)


