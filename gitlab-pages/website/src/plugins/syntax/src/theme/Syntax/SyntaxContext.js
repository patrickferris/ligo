import React from 'react';

const SyntaxContext = React.createContext({syntax: "jsligo", setSyntax: () => {} });

export default SyntaxContext;
